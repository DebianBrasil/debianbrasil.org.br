---
title: "Debian Day 2024 em Belém e Poços de Caldas - Brasil"
kind: article
created_at: 2024-08-28 11:00
author: Paulo Henrique de Lima Santana (phls)
---

por Paulo Henrique de Lima Santana (phls)

Listamos abaixo os links para os relatos e notícias do Debian Day 2024 realizado
em Belém e Poços de Caldas:

- [Belém - PA](https://paralivre.org/noticias/primeiro-debianday-belem-celebrando-31-anos-de-debian-com-palestras-e-networking)
- [Poços de Caldas - MG](https://portal.pcs.ifsuldeminas.edu.br/noticias/5270)
