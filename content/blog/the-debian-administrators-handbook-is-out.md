---
title: "The Debian Administrator’s HandBook is out"
language: english
kind: article
created_at: 2012-05-11 20:09
---

Yesterday, [Raphaël Hertzog](http://raphaelhertzog.com/2012/05/10/the-debian-administrators-handbook-is-available),
announced that
[The Debian Administrator’s HandBook](http://debian-handbook.info/get/)
is available.

[![](http://debian-handbook.info/files/2012/04/front-cover-232x300.png)](http://debian-handbook.info/files/2012/04/front-cover-232x300.png)


The Debian Administrator’s HandBook is a book written by Raphaël Hertzog and
Roland Mas, two Debian Developers. It was originally  written in french, and
now it has been translated into english.

You can browse the online version for free, or you can pay for it and get the
paperback, or e-book version (in 3 formats: PDF, EPUB, Mobipocket)

The book source is also [available](http://debian-handbook.info/contribute/)
for anyone who wants to contribute to the project.

With the book source available, we can expect a brazilian portuguese translation
someday (as some people already commented on Raphaël’s announcement). As well as
translations to other languages.

Fonte:
[http://earruda.eti.br/blog/2012/05/the-debian-administrators-handbook-is-out/](http://earruda.eti.br/blog/2012/05/the-debian-administrators-handbook-is-out/)
