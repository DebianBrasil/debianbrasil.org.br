---
title: "How were the Debian activities at FGSL 2017 in Goiânia"
language: english
kind: article
created_at: 2017-11-22 13:10
author: Paulo Henrique de Lima Santana
---

![Banner fgsl debian](/blog/imagens/banner-fgsl-debian.png =400x)

On November 17 and 18, 2017, the
[XIV Goiano Free Software Forum (FGSL)](http://2017.fgsl.net) was held at the
Federal University of Goias (UFG) in Goiânia.

The Debian Community was present at the FGSL to make 3 talks:

**Control of anomalies and blocking attacks in real-time networks**

- [João Eriberto Mota Filho](http://eriberto.pro.br)
- [File](http://www.eriberto.pro.br/palestras/controle-anomalias-rede.pdf)

**The Debian Project wants you!**

- [Paulo Henrique de Lima Santana](http://phls.com.br/)
- [File](https://gitlab.com/phls/arquivos-de-apresentacoes/-/blob/master/2017-11-18-FGSL-o-projeto-debian-quer-voce.pdf)

**Debian Backstage: understand how the distribution works**

- [João Eriberto Mota Filho](http://eriberto.pro.br)
- [File](http://www.eriberto.pro.br/palestras/bastidores_debian.pdf)

Before the event the organization published many images on social networks to
help in spreading confirmed activities, like these below:

![Banner Eriberto](/blog/imagens/banner-fgsl-eriberto-1.png =400x)

![Banner Paulo](/blog/imagens/banner-fgsl-paulo.png =400x)

In addition to the talks, the Debian Community had a booth to keep in touch with
attendees and to sell several Debian branded merchandise produced by
[Curitiba Free Community](http://curitibalivre.org.br/). Just to remember that
the profit is reverted to organize future Free Software events.

The Debian Community booth with Rodrigo Troian, Paulo Santana (phls) and
Christiane Borges - photo from Álvaro Justen (CC BY-SA 2.0)

![Mesa da comunidade Debian](/blog/imagens/mesa-debian-01.jpg =400x)

Debian branded merchandise - photos from Álvaro Justen (CC BY-SA 2.0)

![Mesa da comunidade Debian com produtos](/blog/imagens/mesa-debian-03.jpg =400x)

Alice, Prof. Fábio's daughter - photo from Álvaro Justen (CC BY-SA 2.0)

![Alice](/blog/imagens/menina.jpg =400x)

Paulo Santana talking about "The Debian Project wants you!" - photo from Álvaro
Justen (CC BY-SA 2.0)

![Palestra Paulo](/blog/imagens/paulo-palestra-01.jpg =400x)

João Eriberto talking about "Debian Backstage: understand how the distribution
works" - photos from Paulo Santana (CC BY-SA 4.0)

![Palestra Eribeto](/blog/imagens/eribeto-palestra-01.jpg =400x)

Attendees at XIV FGSL - photo from Álvaro Justen (CC BY-SA 2.0)

![Foto todos no FGSL](/blog/imagens/foto-fgsl-todos.jpg =400x)

You can see more photos here:
<https://www.flickr.com/photos/debianbrasil/albums/72157716020023033>

We are very grateful to the organizers of XIV FGSL for the invitations and the
availability of space for our activities, especially thank you to Prof.
Christiane Borges and to Prof. Marcelo Akira. Congratulations to the
organization for the success of XIV FGSL.