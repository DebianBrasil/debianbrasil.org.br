---
title: "MiniDebConf Curitiba 2016: chamada de atividades"
kind: article
created_at: 2016-01-07 13:28
author: Paulo Henrique de Lima Santana
---

Nos dias 05 e 06 de março acontecerá a **MiniDebConf Curitiba 2016** na sede
de [Aldeia Coworking](http://aldeiaco.com.br/) em Curitiba - Paraná.

Estamos recebendo propostas de palestras e workshops para os dois dias do
evento, obviamente com temas relacionados ao Debian :-)

As palestras podem ser de todos os níveis, mas tenha em mente que o público
alvo principal são os participantes que estão começando o seu contato com o
Debian e querem aprender mais.

Já os workshops são atividades do tipo mão-na-massa voltado para os
participantes mais experientes que poderão contribuir em alguma área como
empacotamento, tradução, solução de bugs, etc.

Nos próximos dias vamos lançar o site oficial da MiniDebConf, mas as propostas
de atividades podem ser enviadas pela página wiki da organização:

<https://wiki.debian.org/Brasil/Eventos/MiniDebConfCuritiba2017/Atividades/CFP>

Esperamos propostas de todos, desde usuários até Desenvolvedores oficiais
Debian.
