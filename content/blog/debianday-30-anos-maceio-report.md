---
title: "Debian Day 30 anos in Maceió - Brazil"
language: english
kind: article
created_at: 2023-09-11 05:00
author: Paulo Henrique de Lima Santana (phls)
---

The Debian Day in Maceió 2023 took place at the Senai auditorium in Maceió with
the support and organization of [Oxe Hacker Club](http://oxehacker.club/).

There were around 90 people registered, and 40 ateendees present on Saturday to
participate in the event, which featured the following 6 talks:

- Debian Package - Daniel Pimentel
- Attacking Linux EDRs for Fun and Profit - Tiago Peixoto
- Docker: Introdução ao mundo dos containers - Baltazar
- Hardening, Debian e CIS Benchmarks - Moises
- Carreira e Software Livre em Cyber Security - Edo
- O Software Livre já pode pagar minhas contas? - Gilberto Martins

Debian Day also had an install fest and unconference (random chat,
food and drinks).

![Debian Day Maceió 2023 1](/blog/imagens/debianday-maceio-2023-1.jpg =400x)

![Debian Day Maceió 2023 1](/blog/imagens/debianday-maceio-2023-2.jpg =400x)

![Debian Day Maceió 2023 1](/blog/imagens/debianday-maceio-2023-3.jpg =400x)

![Debian Day Maceió 2023 1](/blog/imagens/debianday-maceio-2023-4.jpg =400x)

![Debian Day Maceió 2023 1](/blog/imagens/debianday-maceio-2023-5.jpg =400x)

![Debian Day Maceió 2023 1](/blog/imagens/debianday-maceio-2023-6.jpg =400x)

![Debian Day Maceió 2023 1](/blog/imagens/debianday-maceio-2023-7.jpg =400x)

![Debian Day Maceió 2023 1](/blog/imagens/debianday-maceio-2023-8.jpg =400x)

![Debian Day Maceió 2023 1](/blog/imagens/debianday-maceio-2023-9.jpg =400x)

![Debian Day Maceió 2023 1](/blog/imagens/debianday-maceio-2023-10.jpg =400x)

![Debian Day Maceió 2023 1](/blog/imagens/debianday-maceio-2023-11.jpg =400x)

