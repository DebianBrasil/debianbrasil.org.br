---
title: "Quem somos"
kind: page
created_at: 2020-09-13 20:09
author: Paulo Henrique de Lima Santana
---

# O que é a Comunidade Debian Brasil

A [Comunidade Debian Brasil](http://debianbrasil.org.br/) é um projeto formado
por usuários(as) do sistema operacional Debian, e por pessoas que colaboram
para o Projeto Debian como
[tradutores(as)](https://wiki.debian.org/Brasil/Traduzir),
documentadores(as), designers, organizadores de
[eventos](https://wiki.debianbrasil.org.br/eventos),
[mantenedores(as)](https://nm.debian.org/public/people/dm_all) e
[desenvolvedores(as)](https://nm.debian.org/public/people/dd_all) de pacotes
de software, conhecidos(as) respectivamente
como DM - Debian Maintainer e DD - Debian Developer.
E que tem como objetivo central tornar o [Projeto Debian](http://www.debian.org/)
mais próximo dos(as) usuários(as) brasileiros(as), e o sistema operacional
Debian e sua documentação traduzidos para o português do Brasil. Todas essas
pessoas desenvolvem seus trabalhos de forma voluntária.

Você pode começar lendo sobre
[o que é o Debian](/o-que-e-debian). Se você é um(a)
usuário(a), comece pela seção
[documentos](https://wiki.debian.org/Brasil/Documentos), onde você encontrará
diversos textos escritos por voluntários do projeto ou traduzidos,
principalmente a partir de originais do
[Projeto de Documentação do Debian (DDP - Debian Documentation Project)](https://www.debian.org/doc/ddp).
Se você está a fim de colaborar, leia nossa seção como
[colaborar](/como-colaborar).

Faça parte da nossa comunidade assinando nossas
[listas de discussão](https://wiki.debianbrasil.org.br/listas) e batendo um papo
nos nossos [canais de IRC](https://wiki.debianbrasil.org.br/irc) e nos
[grupos do matrix e telegram](https://wiki.debianbrasil.org.br/matrix-telegram).
