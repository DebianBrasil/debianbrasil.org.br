---
title: "Debian Zine"
kind: page
created_at: 2020-09-13 20:09
author: Paulo Henrique de Lima Santana
---

# Debian Zine

Fanzine Eletrônico sobre o Debian em português. Foram publicadas 4 edições.

### Número 0 - novembro de 2004 - Conisli

[PDF versão colorida.](/zine/debianzine_0.pdf)

### Número 1 - março de 2005

[PDF versão colorida.](/zine/debianzine_2005_001_co.pdf)

### Número 2 - junho de 2005 - FISL

[PDF versão colorida.](/zine/debianzine_2005_002_fisl_co_full.pdf)

### Número 3 - outubro de 2005

[PDF versão colorida.](/zine/debianzine_2005_003_on.pdf)
